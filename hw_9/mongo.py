import os
import pymongo


MONGO_HOST = os.environ.get('MONGO_HOST', 'localhost')
client = pymongo.MongoClient(f'mongodb://root:example@{MONGO_HOST}:27017')
database = client['crm_db']
contacts_collection = database['contact']


def add_contacts(name, email, phone_number):
    contact_ids = contacts_collection.insert_one(
        {"name": name, "email": email, "phone_number": phone_number}).inserted_id
    return contact_ids


def show_contact(obj_id):
    return contacts_collection.find_one({"_id": obj_id})