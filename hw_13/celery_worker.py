from celery import Celery
from mail_settings import EmailWrapper
from models import EmailCreds
import alchemy_db
import os

RABBIT_HOST = os.environ.get('RABBIT_HOST', 'localhost')
app = Celery('celery_worker', broker=f'pyamqp://guest@{RABBIT_HOST}//')


@app.task
def send_mail(id_email_creds, receiver_email, email_message):
    alchemy_db.init_db()
    email_creds_info = alchemy_db.db_session.query(EmailCreds).get(id_email_creds)
    email_wrapper = EmailWrapper(**email_creds_info.get_mail_info())
    email_wrapper.send_mail(receiver_email, email_message)
