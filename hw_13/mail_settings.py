import email
import imaplib
import smtplib, ssl
import poplib
import email.header
from email import policy, parser


class EmailWrapper:
    def __init__(self, user_email, login, password, smtp_server, smtp_port=465, pop_server=None, pop_port=995,
                 imap_server=None, imap_port=993):
        self.user_email = user_email
        self.login = login
        self.password = password
        self.smtp_server = smtp_server
        self.smtp_port = smtp_port
        self.pop_server = pop_server
        self.pop_port = pop_port
        self.imap_server = imap_server
        self.imap_port = imap_port

    def send_mail(self, receiver_email, email_message):
        context = ssl._create_unverified_context()
        smtplib.SMTP_SSL(self.smtp_server, 465, context=context)
        with smtplib.SMTP_SSL(self.smtp_server, 465, context=context) as server:
            server.login(self.user_email, self.password)
            server.sendmail(self.user_email, receiver_email, email_message)

    def get_emails(self, protocol='imap'):
        if protocol == 'pop3':
            return self.get_pop3()
        elif protocol == 'imap':
            return self.get_imap()
        else:
            raise ValueError('Unknown protocol')

    def get_pop3(self):
        M = poplib.POP3_SSL(self.pop_server, self.pop_port)
        M.user(self.user_email)
        M.pass_(self.password)
        messages = [M.retr(i) for i in range(1, len(M.list()[1]) + 1)]
        messages = ['\n'.join(map(bytes.decode, mssg[1])) for mssg in messages]
        messages = [parser.Parser().parsestr(mssg) for mssg in messages]
        mail_list = []
        for message in messages:
            for part in message.walk():
                if part.get_content_type():
                    body = part.get_payload(decode=True)
            mail_list.append([message['from'], message['subject'], body, message['date']])
        M.quit()
        return mail_list

    def get_imap(self):
        M = imaplib.IMAP4_SSL(self.imap_server, self.imap_port)
        M.login(self.user_email, self.password)
        M.select("INBOX")
        result, data = M.search(None, "ALL")
        ids = data[0]
        id_list = ids.split()
        mail_list = []
        for email_id in id_list:
            result, data = M.fetch(email_id, "(RFC822)")
            raw_email = data[0][1]
            raw_email_string = raw_email.decode('utf-8')
            email_message = email.message_from_string(raw_email_string, policy=policy.default)
            letter_from = email.utils.parseaddr(email_message['From'])
            letter_subject = email_message['Subject']
            date = email_message['Date']
            if email_message.is_multipart():
                for payload in email_message.get_payload():
                    body = payload.get_payload(decode=True).decode('utf-8')
            else:
                body = email_message.get_payload(decode=True).decode('utf-8')
            mail_list.append([letter_from[1], letter_subject, body, date])
        M.close()
        return mail_list
