"""empty message

Revision ID: f6288c4d54ef
Revises: ec62e68cb237
Create Date: 2023-03-08 00:18:01.334509

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f6288c4d54ef'
down_revision = 'ec62e68cb237'
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
