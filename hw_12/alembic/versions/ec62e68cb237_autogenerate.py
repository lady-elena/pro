"""autogenerate

Revision ID: ec62e68cb237
Revises: 
Create Date: 2023-03-01 18:57:54.526697

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ec62e68cb237'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
