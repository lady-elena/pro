from threading import Thread
from multiprocessing import Process
from datetime import datetime


text = []
for i in range(40000000):
    text.append(i)
text = str(text)


def write_to_file(data, tag="thread"):
    with open(f'text_{tag}.txt', 'w') as f:
        f.write(data)


if __name__ == '__main__':
    start = datetime.now()
    write_to_file(text)
    end = datetime.now()
    print(f'Write time in one thread is {end - start}')

    thread_1 = Thread(target=write_to_file, kwargs={'data': text, 'tag': 'thread_1'})
    thread_2 = Thread(target=write_to_file, kwargs={'data': text, 'tag': 'thread_2'})

    start_threads = datetime.now()

    thread_1.start()
    thread_2.start()
    thread_1.join()
    thread_2.join()

    end_threads = datetime.now()
    print(f'Write time in two threads is {end_threads - start_threads}') #приблизно той же час, що в один потiк

    process_1 = Process(target=write_to_file, kwargs={'data': text, 'tag': 'process_1'})
    process_2 = Process(target=write_to_file, kwargs={'data': text, 'tag': 'process_2'})

    start_processes = datetime.now()

    process_1.start()
    process_2.start()
    process_1.join()
    process_2.join()

    end_processes = datetime.now()
    print(f'Write time in two processes is {end_processes - start_processes}') #не ефективно при роботi input-output
