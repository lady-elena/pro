import datetime
import threading
from multiprocessing import Process


def count_lucky_tickets(start, stop):
    result = 0
    for number in range(start, stop):
        first = 0
        second = 0
        first += int(number // 100000)
        number = number % 100000
        first += int(number // 10000)
        number = number % 10000
        first += int(number // 1000)
        number = number % 1000
        second += int(number // 100)
        number = number % 100
        second += int(number // 10)
        number = number % 10
        second += int(number % 10)
        if first == second:
            result += 1
    return result


if __name__ == '__main__':
    datetime_1 = datetime.datetime.now()
    count_lucky_tickets(0, 1000000)
    datetime_2 = datetime.datetime.now()
    print("Count lucky tickets in one thread for", datetime_2 - datetime_1)

    thread1 = threading.Thread(target=count_lucky_tickets, args=(0, 500000,))
    thread2 = threading.Thread(target=count_lucky_tickets, args=(500000, 1000000,))

    datetime_1 = datetime.datetime.now()
    thread1.start()
    thread2.start()

    thread1.join()
    thread2.join()
    datetime_2 = datetime.datetime.now()
    print(f'Count lucky tickets in two threads for {datetime_2 - datetime_1}')

    p1 = Process(target=count_lucky_tickets, args=(0, 500000, ))
    p2 = Process(target=count_lucky_tickets, args=(500000, 1000000, ))
    datetime_1 = datetime.datetime.now()
    p1.start()
    p2.start()
    p1.join()
    p2.join()
    datetime_2 = datetime.datetime.now()
    print(f'Count lucky tickets in two processes for {datetime_2 - datetime_1}')