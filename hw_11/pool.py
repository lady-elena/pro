import datetime
import multiprocessing
from multiprocessing.pool import ThreadPool


def count_lucky_tickets(start, stop):
    res = 0
    for number in range(start, stop):
        first = 0
        second = 0
        first += int(number // 100000)
        number = number % 100000
        first += int(number // 10000)
        number = number % 10000
        first += int(number // 1000)
        number = number % 1000
        second += int(number // 100)
        number = number % 100
        second += int(number // 10)
        number = number % 10
        second += int(number % 10)
        if first == second:
            res += 1
    return res


if __name__ == '__main__':
    pool = ThreadPool(processes=2)
    datetime_1 = datetime.datetime.now()

    thread_1 = pool.apply_async(count_lucky_tickets, (0, 500000))
    thread_2 = pool.apply_async(count_lucky_tickets, (500000, 1000000))

    result_1 = thread_1.get()
    result_2 = thread_2.get()
    result = result_1 + result_2

    datetime_2 = datetime.datetime.now()

    print(f'The number of lucky tickets is {result}')
    print(f'Count lucky tickets in two threads for {datetime_2 - datetime_1}')

    pool = multiprocessing.pool.Pool(processes=2)
    datetime_1 = datetime.datetime.now()

    process_1 = pool.apply_async(count_lucky_tickets, (0, 500000))
    process_2 = pool.apply_async(count_lucky_tickets, (500000, 1000000))

    res_1 = process_1.get()
    res_2 = process_2.get()
    res = res_1 + res_2

    datetime_2 = datetime.datetime.now()

    print(f'The number of lucky tickets is {res}')
    print(f'Count lucky tickets in two processes for {datetime_2 - datetime_1}')
