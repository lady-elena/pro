"""first

Revision ID: b47d2c11c1b9
Revises: f9f89e9b1629
Create Date: 2023-02-25 01:31:38.594677

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b47d2c11c1b9'
down_revision = 'f9f89e9b1629'
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
